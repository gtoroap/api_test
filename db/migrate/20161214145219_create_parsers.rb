class CreateParsers < ActiveRecord::Migration[5.0]
  def change
    create_table :parsers do |t|
      t.string :url
      t.text :content
      t.text :links

      t.timestamps
    end
  end
end
